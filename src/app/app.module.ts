import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainNavigationComponent } from './main-navigation/main-navigation.component';
import { PageSuperHerosComponent } from './main-navigation/pages/page-super-heros/page-super-heros.component';
import { PageEquipesComponent } from './main-navigation/pages/page-equipes/page-equipes.component';
import { RouterModule, Routes, Router} from '@angular/router';
import {  HttpClientModule } from '@angular/common/http';
import { MatListModule } from '@angular/material/list';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { FlexLayoutModule } from '@angular/flex-layout';
import { DetailInfoSuperHeroComponent } from './main-navigation/pages/page-super-heros/components/detail-info-super-hero/detail-info-super-hero.component';
import { DialogSauvegarderChargerEquipeComponent } from './main-navigation/pages/page-equipes/components/dialog-sauvegarder-charger-equipe/dialog-sauvegarder-charger-equipe.component';
import { DialogActionCreerEquipeComponent } from './main-navigation/pages/page-equipes/components/dialog-action-creer-equipe/dialog-action-creer-equipe.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatTooltipModule} from '@angular/material/tooltip';



const routes: Routes = [
  { path: '', redirectTo: '/super-heros', pathMatch: 'full' },
  {path: 'super-heros', component: PageSuperHerosComponent, },
  {path: 'equipes', component: PageEquipesComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    MainNavigationComponent,
    PageSuperHerosComponent,
    PageEquipesComponent,
    DetailInfoSuperHeroComponent,
    DialogSauvegarderChargerEquipeComponent,
    DialogActionCreerEquipeComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule, 
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    HttpClientModule,
    MatListModule,
    MatDialogModule,
    FlexLayoutModule,
    MatSnackBarModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
