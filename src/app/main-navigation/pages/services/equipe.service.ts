import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Equipe } from '../models/equipe.model';
import { SuperHero } from '../models/superHeroes.model';

@Injectable({
  providedIn: 'root'
})
export class EquipeService {

  equipe: Equipe = {
    heros: []
  };

  constructor(
    private snackbar: MatSnackBar
  ) { }

  /** ajoute un super-héros à l'équipe */
  public ajouterSuperHeros(hero: SuperHero) {
    const estDejaMembre = this.equipe.heros.some(h => h.id == hero.id)
    if (!estDejaMembre) {
      this.equipe.heros.push(hero);
    }
  }

  /** supprime un super-héro de l'équipe */
  public supprimerSuperHero(hero: SuperHero) {
    const index = this.equipe.heros.findIndex(h => h.id == hero.id)
    this.equipe.heros.splice(index, 1);
  }

  /** retourne l'équipe actuelle */
  public getEquipe() {
    return this.equipe;
  }

  public setEquipe(equipe: Equipe) {
    this.equipe = equipe;
  }

  /** sauvegarde l'équipe dans le localstorage à partir de son nom */
  public sauvegarderEquipe() {
    localStorage.setItem(this.equipe.nomEquipe, JSON.stringify(this.equipe));
    this.snackbar.open('Ton équipe a bien été sauvegardée', '', {
      duration: 5000,
    })
  }

  /** charge une équipe depuis le localstorage à partir d'un nom saisi */
  public chargerEquipe(nom: string) {
    const equipeJson = localStorage.getItem(nom);
    // si une équipe a été trouvée
    if (equipeJson) {
      this.equipe = JSON.parse(equipeJson);
      this.snackbar.open('L\'équipe a bien été chargée', '', {
        duration: 2500,
      })
    } else {
      // sinon on renvoie une équipe vide
      this.equipe = {
        heros: []
      };
      this.snackbar.open('Aucune équipe trouvée pour le nom ' + nom, '', {
        duration: 2500,
      })
    }

  }

  /** supprimer/reset l'équipe actuelle */
  public supprimerEquipe() {
    this.equipe = {
      heros: []
    };
  }
}
