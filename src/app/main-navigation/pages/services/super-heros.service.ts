import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators'
import { Comic } from '../models/comic.model';
import { SuperHero } from '../models/superHeroes.model';

@Injectable({
  providedIn: 'root'
})
export class SuperHerosService {
  public_key = '3a414eb262a7648c2c98bf473bb3e5e8';
  hash = 'c2bed06ed216e44e87ce2a60fc8f276f';

  heroes: SuperHero[]

  constructor(
    private httpClient: HttpClient
  ) { }

  public getListesSuperHeros(offset: number) {
    const url = `https://gateway.marvel.com/v1/public/characters?ts=1&apikey=${this.public_key}&hash=${this.hash}&limit=100&offset=${offset}`
    return this.httpClient.get<SuperHero[]>(url).pipe(map((data: any) => data.data)).toPromise()
  }

  public async setListeSuperHeros() {
    let api;
    let offset = 0;

    this.heroes = []
    // récupère les 100 premiers héros de l'API
    api = await this.getListesSuperHeros(0)
    this.heroes.push(...api.results);

    // récupère par paquets de 100 tous les héros de l'API
    while (offset <= api.total) {
      offset +=100;
      api = await this.getListesSuperHeros(offset);
      this.heroes.push(...api.results);
    }
  }

  public getListeComicsParSuperHeros(id: number): Promise<Comic[]> {
    const urlComic = `http://gateway.marvel.com/v1/public/characters/${id}/comics?ts=1&apikey=${this.public_key}&hash=${this.hash}&limit=100`
    return this.httpClient.get<Comic[]>(urlComic).pipe(map((data: any) => data.data.results)).toPromise()
  }
}
