import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-action-creer-equipe',
  templateUrl: './dialog-action-creer-equipe.component.html',
  styleUrls: ['./dialog-action-creer-equipe.component.css']
})
export class DialogActionCreerEquipeComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DialogActionCreerEquipeComponent>
  ) { }

  ngOnInit(): void {
  }
}
