import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogActionCreerEquipeComponent } from './dialog-action-creer-equipe.component';

describe('DialogActionCreerEquipeComponent', () => {
  let component: DialogActionCreerEquipeComponent;
  let fixture: ComponentFixture<DialogActionCreerEquipeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogActionCreerEquipeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogActionCreerEquipeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
