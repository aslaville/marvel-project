import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-sauvegarder-charger-equipe',
  templateUrl: './dialog-sauvegarder-charger-equipe.component.html',
  styleUrls: ['./dialog-sauvegarder-charger-equipe.component.css']
})
export class DialogSauvegarderChargerEquipeComponent implements OnInit {

  champNom: FormControl = new FormControl('');

  constructor(
    public dialogRef: MatDialogRef<DialogSauvegarderChargerEquipeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {title: string, texte: string}
  ) { }

  ngOnInit(): void {
  }

  /* Si l'utilisateur choisit le bouton annuler */
  onNoClick(): void {
    this.champNom.setValue(null);
    this.dialogRef.close();
  }

}
