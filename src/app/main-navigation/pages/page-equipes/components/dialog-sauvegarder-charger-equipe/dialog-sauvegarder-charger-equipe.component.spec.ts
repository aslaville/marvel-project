import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogSauvegarderChargerEquipeComponent } from './dialog-sauvegarder-charger-equipe.component';

describe('DialogSauvegarderEquipeComponent', () => {
  let component: DialogSauvegarderChargerEquipeComponent;
  let fixture: ComponentFixture<DialogSauvegarderChargerEquipeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogSauvegarderChargerEquipeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogSauvegarderChargerEquipeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
