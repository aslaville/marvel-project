import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Equipe } from '../models/equipe.model';
import { SuperHero } from '../models/superHeroes.model';
import { EquipeService } from '../services/equipe.service';
import { DialogActionCreerEquipeComponent } from './components/dialog-action-creer-equipe/dialog-action-creer-equipe.component';
import { DialogSauvegarderChargerEquipeComponent } from './components/dialog-sauvegarder-charger-equipe/dialog-sauvegarder-charger-equipe.component';

@Component({
  selector: 'app-page-equipes',
  templateUrl: './page-equipes.component.html',
  styleUrls: ['./page-equipes.component.css']
})
export class PageEquipesComponent implements OnInit {

  equipe: Equipe;

  constructor(
    private equipeService: EquipeService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.equipe = this.equipeService.getEquipe();
  }

  /** permet de récupérer l'image d'un super-hero */
  public getImage(hero: SuperHero) {
    return hero.thumbnail.path + '.' + hero.thumbnail.extension
  }

  /** Supprime le hero choisi de l'equipe */
  public supprimeHerosEquipe(hero: SuperHero) {
    this.equipeService.supprimerSuperHero(hero)
    this.equipe = this.equipeService.getEquipe()
  }

  /** Sauvegarde l'equipe actuelle */
  public sauvegarderEquipe(supprime: boolean) {

    // Ouvre le composant dialog pour saisir le nom de l'équipe à sauvegarder
    const dialogRef = this.dialog.open(DialogSauvegarderChargerEquipeComponent, {
      data: {
        title: "Sauvegarder l'équipe",
        texte: "Veuillez saisir un nom d'équipe pour pouvoir la sauvegarder :"
      }
    });

    dialogRef.afterClosed().subscribe(nom => {

      // si un nom a été saisi après le choix dans la modale
      if (nom) {
        this.equipe.nomEquipe = nom;
        this.equipeService.setEquipe(this.equipe);
        this.equipeService.sauvegarderEquipe();
        // s'il faut aussi supprimer l'equipe
        if(supprime){
          this.supprimerEquipe()
        }
      }
    })

  }

  /** Permet de supprimer l'equipe */
  public supprimerEquipe() {

    this.equipeService.supprimerEquipe();
    this.equipe = this.equipeService.getEquipe();
  }

  /** Permet de créer une nouvelle équipe */
  public creerEquipe() {
    // Ouvre le composant dialog pour choisir quoi faire de l'équipe actuelle
    const dialogRef = this.dialog.open(DialogActionCreerEquipeComponent);

    dialogRef.afterClosed().subscribe(action => {
      if (action == "sauvegarder") {
        this.sauvegarderEquipe(true);
      } else {
        this.supprimerEquipe()
      }

    })

  }


  /** Permet de charger une équipe sauvegardée */
  public chargerEquipe() {
    const dialogRef = this.dialog.open(DialogSauvegarderChargerEquipeComponent, {
      data: {
        title: "Charger une équipe",
        texte: "Veuillez saisir le nom de l'équipe que vous souhaitez charger :"
      }
    });

    dialogRef.afterClosed().subscribe(nom => {
      // si un nom a été saisi après le choix dans la modale
      if (nom) {
        this.equipeService.chargerEquipe(nom);
        this.equipe = this.equipeService.getEquipe();
      }

    })
  }

}
