import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageSuperHerosComponent } from './page-super-heros.component';

describe('PageSuperHerosComponent', () => {
  let component: PageSuperHerosComponent;
  let fixture: ComponentFixture<PageSuperHerosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageSuperHerosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageSuperHerosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
