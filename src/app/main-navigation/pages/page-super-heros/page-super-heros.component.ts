import { AfterViewInit, Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SuperHero } from '../models/superHeroes.model';
import { EquipeService } from '../services/equipe.service';
import { SuperHerosService } from '../services/super-heros.service';
import { DetailInfoSuperHeroComponent } from './components/detail-info-super-hero/detail-info-super-hero.component';


@Component({
  selector: 'app-page-super-heros',
  templateUrl: './page-super-heros.component.html',
  styleUrls: ['./page-super-heros.component.css']
})
export class PageSuperHerosComponent implements OnInit, AfterViewInit {

  /** Chargement des héros  */
  loadHeroes = true;

  /** liste de tous les super-héros */
  heroes: SuperHero[];

  pageCourante = 1;

  elementParPage = 10

  nbPage: number;

  /** liste des super héros à afficher sur une page */
  listeHeroesParPage: SuperHero[]

  rechercheCtrl: FormControl = new FormControl('')

  constructor(
    private superHerosService: SuperHerosService,
    private equipeService: EquipeService,
    private dialog: MatDialog,
    private snackbar: MatSnackBar
  ) { }

  async ngOnInit(): Promise<void> {
    // initialise la liste des super-héros au démarrage de l'application
    if (!this.superHerosService.heroes) {
     await this.superHerosService.setListeSuperHeros();
    }

    this.heroes = this.superHerosService.heroes;
    this.loadHeroes = false;

    // attribue le nombre de page selon le nombre de héros
    this.nbPage = Math.ceil(this.heroes.length/this.elementParPage)
    // récupère les premiers super-héros à afficher
    this.listeHeroesParPage = this.heroes.slice(0, this.elementParPage)
  
  }

  ngAfterViewInit() {
    // effectue une recherche selon la valeur du champ à chaque changement de valeur
    this.rechercheCtrl.valueChanges.subscribe(() => {
      this.rechercher()
    })
  }

  /** récupère le chemin de l'image d'un super-héro */
  public getImage(hero: SuperHero) {
    return hero.thumbnail.path + '.' + hero.thumbnail.extension
  }

  /** ajoute un super-héros à une équipe */
  ajouterHeros(hero: SuperHero) {
    this.equipeService.ajouterSuperHeros(hero)
    this.snackbar.open(hero.name + ' vient d\être ajouté à ton équipe', '', {
      duration: 5000,
    })
  }

  /** ouvre la modale du détail du super-héro */
  ouvrirDetail(hero: SuperHero) {
    this.dialog.open(DetailInfoSuperHeroComponent, {data: hero})
  }

  /** recherche parmi tous les héros selon la valeur du champ de recherche */
  rechercher() {
    this.heroes = this.superHerosService.heroes
    this.heroes = this.heroes.filter(h => h.name.toLowerCase().includes(this.rechercheCtrl.value.toLowerCase().trim()))

    // redéfini le nombre de pages et de résultats selon la recherche
    this.nbPage = Math.ceil(this.heroes.length/this.elementParPage)
    this.listeHeroesParPage = this.heroes.slice(0, this.elementParPage)
  }

  /** Affiche 10 héros aléatoirement */
  rechercherAleatoire(){
    let listeHeroesAleatoire: SuperHero[] = [];
    
    const copieListeHeros: SuperHero[] = this.heroes;
    for (let index = 0; index < 10; index++) {
      const num = this.getRandomInt(copieListeHeros.length);
      const hero = copieListeHeros[num];
      listeHeroesAleatoire.push(hero);
      copieListeHeros.slice(num,num+1);
      
    }

    this.listeHeroesParPage = listeHeroesAleatoire;

  }

  /** Permet de passer à la page suivante */
  changerPageSuperieur() {
    this.pageCourante = this.pageCourante < this.nbPage ? this.pageCourante + 1: this.pageCourante
    this.listeHeroesParPage = this.heroes.slice(this.pageCourante*this.elementParPage-this.elementParPage, this.pageCourante*this.elementParPage)
  }

    /** Permet de revenir à la page précédente */
  changerPageInferieur() {
    this.pageCourante = this.pageCourante > 1 ? this.pageCourante - 1 : this.pageCourante
    this.listeHeroesParPage = this.heroes.slice(this.pageCourante*this.elementParPage-this.elementParPage, this.pageCourante*this.elementParPage)
  }

  /** retourne un nombre aléatoire */
  private getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

}
