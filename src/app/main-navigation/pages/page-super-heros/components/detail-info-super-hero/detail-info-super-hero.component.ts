import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Comic } from '../../../models/comic.model';
import { SuperHero } from '../../../models/superHeroes.model';
import { EquipeService } from '../../../services/equipe.service';
import { SuperHerosService } from '../../../services/super-heros.service';

@Component({
  selector: 'app-detail-info-super-hero',
  templateUrl: './detail-info-super-hero.component.html',
  styleUrls: ['./detail-info-super-hero.component.css']
})

export class DetailInfoSuperHeroComponent implements OnInit {

  premierComic : Comic;
  dernierComic : Comic;

  constructor(
    private dialogRef: MatDialogRef<DetailInfoSuperHeroComponent>,
    private equipeService: EquipeService,
    private snackbar: MatSnackBar,
    private superHerosService: SuperHerosService,
    @Inject(MAT_DIALOG_DATA) public hero: SuperHero
  ) { }

  async ngOnInit(): Promise<void> {
    this.dialogRef.disableClose = true
    /** récupère les comics du super-héros */
    const listeComics = await this.superHerosService.getListeComicsParSuperHeros(this.hero.id)
    this.premierComic = listeComics[listeComics.length-1];
    this.dernierComic = listeComics[0];
  }

  public getImage(hero: SuperHero) {
    return hero.thumbnail.path + '.' + hero.thumbnail.extension
  }

  /** ajoute le héros à l'équipe */
  public ajouterHeros(hero: SuperHero) {
    this.equipeService.ajouterSuperHeros(hero)
    this.dialogRef.close()
    // Message pour l'utilisateur
    this.snackbar.open(hero.name + ' vient d\être ajouté à ton équipe', '', {
      duration: 5000,
    })
  }

}
