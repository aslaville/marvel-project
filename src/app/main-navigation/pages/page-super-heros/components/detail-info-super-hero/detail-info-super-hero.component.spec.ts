import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailInfoSuperHeroComponent } from './detail-info-super-hero.component';

describe('DetailInfoSuperHeroComponent', () => {
  let component: DetailInfoSuperHeroComponent;
  let fixture: ComponentFixture<DetailInfoSuperHeroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailInfoSuperHeroComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailInfoSuperHeroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
