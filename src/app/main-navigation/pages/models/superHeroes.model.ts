export interface SuperHero {
    id: number;
    name: string;
    description: string;
    thumbnail: Image
}

export interface Image {
    path: string,
    extension: string
}