
export interface Comic {
    id: number,
    title: string,
    description: string,
    dates: ComicDate[]
}

export interface ComicDate {
    type: string,
    date: Date,
}