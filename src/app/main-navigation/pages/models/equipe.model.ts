import { SuperHero } from "./superHeroes.model";

export interface Equipe {
    nomEquipe?: string,
    heros : SuperHero[],
}