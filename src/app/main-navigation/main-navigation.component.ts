import { R3TargetBinder } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { MatButton } from '@angular/material/button';
import { ActivatedRoute, Router } from '@angular/router';
import { PageEquipesComponent } from './pages/page-equipes/page-equipes.component';

@Component({
  selector: 'main-navigation',
  templateUrl: './main-navigation.component.html',
  styleUrls: ['./main-navigation.component.css']
})
export class MainNavigationComponent implements OnInit {

  logoApp = '/assets/logo.png'

  pageActive = 1;

  constructor() { }

  ngOnInit(): void {
    if (window.location.href == 'http://localhost:4200/equipes') {
      this.pageActive = 2
    }
    
  }

  /** permet de définir la page active et lui ajouter la classe active */
  setPageActive(page: number) {
    this.pageActive = page
    
  }

}
